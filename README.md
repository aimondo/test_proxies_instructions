##############################################################
# Aimondo GmbH (c) 2018
# Fleher Str.32, 40223 Dusseldorf
# Germany
# jobs(at)aimondo.com
# www.aimondo.com
##############################################################

##############################################################
# PURPOSE: Challenge Interviewees to complete a task @ Aimondo 
##############################################################

##############################################################
# TASK:
    1) Import the test_proxies.csv into a Postgresql Database and normalize the data per Country, 
        Channel, Subnet, Proxies and a mapping table to keep the initial mapping from the csv file.
    2) Create a query output and write it as a table into an (unformated) html, 
        where a max output of 100 datasets is present 
        per country per channel and per Subnet. 
        Within each subnet only 5 unique proxy urls are allowed per dataset.

# DONE CRITERIA:
    - Print a visualized DB-Model
    - Import all csv data into the database
    - The mapping in the csv file must be the 
        same as the mapping within the database
    - The output should indicate whether or not the proxy url is ipv6 or not
    - The output query should be below 1 second execution time
    - The html table should reflect as written in task 2)
    - Create a pull-request to the 
        test_proxies repository (https://bitbucket.org/aimondo/test_proxies.git)

##############################################################
# REQUIREMENTS:
    - Use Python 2.7
    - Use Django
    - Use Django DB-Modeling    
    - If applicable:
        - Use SQLAlchemy (ORM) or pure PSQL (Postgres SQL)
        - Use Numpy/Pandas
        - Install other opensource packages if need be



